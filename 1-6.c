// arrays

/* program to count the number of occurrences of each digit, */
/* of white space chars, and of all other chars */

#include <stdio.h>

/* count digits, whitespace, others */

/* main() */
/* { */
    /* int c, i, nwhite, nother; */
    /* int ndigit[10]; */
    /* nwhite = nother = 0; */
    /* for (i = 0; i < 10; i++) */
        /* ndigit[i] = 0; */

    /* while ((c = getchar()) != EOF) */
        /* [> we can use comparison like this because char <] */
        /* [> is just a type of integer in C <] */
        /* if (c >= '0' && c <= '9') */
            /* [> really smart, increments the index of the array <] */
            /* [> according to the digit found <] */
            /* ++ndigit[c-'0'];  */
        /* else if (c == ' ' || c == '\t' || c == '\n') */
            /* ++nwhite; */
        /* else */
            /* ++nother; */
    
    /* printf("\n"); */
    /* printf("digits ="); */
    /* for (i = 0; i < 10; i++) */
        /* printf(" %d", ndigit[i]); */
    /* printf(", white space = %d, other = %d\n", */
            /* nwhite, nother); */
/* } */

// exercise 1-13

/* write a program to print a histogram of the lengths */
/* of words in its input. It is easy to draw the histogram with the bars */
/* horizontal; a vertical orientation is more challenging */

/* main() */
/* { */
    /* int c, length; */
    /* int wlength[20]; */
    /* for (int i = 0; i < 20; i++) */
        /* wlength[i] = 0; */

    /* length = 0; */

    /* while ((c = getchar()) != EOF) { */
        /* if (c == ' ' || c == '\t' || c == '\n') { */
            /* if (length > 0) */
                /* ++wlength[length]; */
            /* length = 0; */
        /* } else { */
            /* ++length; */
        /* } */

    /* } */

    /* printf("Length of words:\n"); */
    /* for (int i = 0; i < 20; i++) { */
        /* printf("%2d | ", i); */
        /* for (int j = 0; j < wlength[i]; j++) */
            /* printf("#"); */
        /* printf("\n"); */
    /* } */
/* } */

// exercise 1-14
int main()
{
    int c;
    int nalphabet[26];

    for (int i = 0; i < 26; i++)
        nalphabet[i] = 0;

    while ((c = getchar()) != EOF) {
        if (c >= 'a' && c <= 'z')
            ++nalphabet[c-'a'];
        if (c >= 'A' && c <= 'Z')
            ++nalphabet[c-'A'];
    }

    for (int i = 0; i < 26; i++) {
        putchar(i + 'a');
        printf(": %2d\n", nalphabet[i]);
    }
}
