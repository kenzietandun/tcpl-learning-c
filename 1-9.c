#include <stdio.h>
#define MAXLINE 1000

/* int get_line(char line[], int maxline); */
void get_line(char line[], int maxline);
void reverse(char to[], char from[]);
void copy(char to[], char from[]);

/* reads the input, then returns the length when newline is reached */
/* the newline character counts as 1 char as well */
/* int get_line(char s[], int lim) */
/* { */
    /* int c, i; */

    /* for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; i++) */
        /* s[i] = c; */
    /* if (c == '\n') { */
        /* s[i] = c; */
        /* ++i; */
    /* } */

    /* [> put the character \0 at the end of the array it is creating <] */
    /* [> to mark the end of the string of chars. <] */
    /* [> the %s format specification in printf expects the corresponding <] */
    /* [> argument to be a string represented in this form <] */
    /* s[i] = '\0'; */
    /* return i; */
/* } */


/* print longest input line */
/* main() */
/* { */
    /* int len; */
    /* int max; */
    /* char line[MAXLINE]; */
    /* char longest[MAXLINE]; */

    /* max = 0; */
    /* while ((len = get_line(line, MAXLINE)) > 0) */
        /* if (len > max) { */
            /* max = len; */
            /* copy(longest, line); */
        /* } */
    /* if (max > 0) */
        /* printf("%s", longest); */
    /* return 0; */
/* } */

/* 1-17
 * write a program to print all input lines that are 
 * longer than 80 characters
 */
/* int main() */
/* { */
    /* int len; */
    /* int max; */
    /* char line[MAXLINE]; */
    /* char longest[MAXLINE]; */

    /* max = 80; */
    /* while ((len = get_line(line, MAXLINE)) > 0) */
        /* if (len > max) { */
            /* printf("%s", line); */
        /* } */
    /* return 0; */
/* } */

/* exercise 1-18
 * write a program to remove trailing blanks and tabs from each
 * line of input, and to delete entirely blank lines
 */
/* int main() */
/* { */
    /* char line[MAXLINE]; */
    /* for (;;) { */
        /* get_line(line, MAXLINE); */
        /* printf("%s", line); */
    /* } */
/* } */

void get_line(char s[], int lim)
{
    int c, i;
    for (i = 0; i < lim-1 && (c = getchar()) != EOF && c != '\n'; i++) {
        s[i] = c;
    }
    s[i] = '\0';
    for (; i > 0; i--) {
        if (s[i] == '\t' || s[i] == ' ' || s[i] == '\0') {
            s[i] = '\0';
        } else {
            i = 0;
        }
    }
}

int main()
{
    char line[MAXLINE];
    char reversed[MAXLINE];
    for (;;) {
        get_line(line, MAXLINE);
        reverse(reversed, line);
        printf("%s", reversed);
    }
}

/* exercise 1-19 */
void reverse(char to[], char from[])
{
    int i, j;
    // get the length of the `from` array
    for (i = 0; from[i] != '\0'; i++)
        ;

    // i now points to the \0 char in the from array
    // decrement it by 1
    --i;

    // now write from index i to 0 to the new array `to`
    for (j = 0; i >= 0; i--) {
        to[j] = from[i];
        j++;
    }
    to[j] = '\0';
}

/* copies `from` to `to` */
void copy(char to[], char from[])
{
    int i;

    i = 0;
    while ((to[i] = from[i]) != '\0')
        ++i;
}
