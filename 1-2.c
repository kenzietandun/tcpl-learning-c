#include <stdio.h>

/* print Fahrenheit-Celsius table
 * for fahr = 0, 20, .. 300 */

/*
main()
{
    int fahr, celsius;
    // by default C initialises int to 0
    int lower, upper, step;

    float test;
    // by default C initialises float to 0.000...

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    while (fahr <= upper) {
        celsius = 5 * (fahr - 32) / 9;
        printf("%3d %6d\n", fahr, celsius);
        fahr = fahr + step;
    }
}
*/

/* print Fahrenheit-Celsius table
 * using float fahr
 * for fahr = 0, 20, .. 300 */

/*
main()
{
    float fahr, celsius;
    // by default C initialises int to 0
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    fahr = lower;
    printf("Fah   Cels\n");
    while (fahr <= upper) {
        celsius = (5.0/9.0) * (fahr - 32.0);
        // %3.0f would print 20  (0 decimal behind)
        // the 3 means the number is at least three chars wide
        //
        // %6.1f would print 4.4 (1 decimal behind)
        printf("%3.0f %6.1f\n", fahr, celsius);
        fahr = fahr + step;
    }
}
*/

/*
 * Print Celsius-Fahr table
 * for celsius = 20, 40, ... 300
 */

main()
{
    float celsius, fahr;
    int lower, upper, step;

    lower = 20;
    upper = 300;
    step = 20;

    celsius = lower;
    while (celsius <= upper) {
        fahr = (celsius + 32) * (9.0 / 5.0);
        printf("%3.0f %6.1f\n", celsius, fahr);
        celsius += step; 
    }
}
