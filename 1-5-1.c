#include <stdio.h>

/* copy input to output; 1st version */

/* main() */
/* { */
    /* // use int here because we need a type  */
    /* // that is big enough to hold any value getchar returns */
    /* // getchar returns the stream of characters + the EOF indicator */
    /* // therefore using char would not work since it would not be */
    /* // big enough to store all of those stuff */
    /* // to test: try compiling with */
    /* // gcc 1-5-1.c -funsigned-char && ./a.out */
    /* // READ MORE HERE: */
    /* // https://stackoverflow.com/questions/35356322/difference-between-int-and-char-in-getchar-fgetc-and-putchar-fputc */
    /* int c; */
    /* // read a character */
    /* c = getchar(); */
    /* // while character is not end of file indicator */
    /* while (c != EOF) { */
        /* // print the character */
        /* putchar(c); */
        /* // read character again */
        /* c = getchar(); */
    /* } */
/* } */


/* copy input to output; 2nd version */
/* main() */
/* { */
    /* int c; */
    
    /* while ((c = getchar()) != EOF) { */
        /* putchar(c); */
    /* } */
/* } */

/* // excercise 1-6 */
/* main() */
/* { */
    /* int c; */
    /* c = getchar(); */
    /* // anything but Ctrl+D will be 0 */
    /* // Ctrl+D (EOF) would return 1 */
    /* printf("%d", c == EOF); */
/* } */


// exercise 1-7
main()
{
    printf("%d", EOF);
    // prints -1
}
