#include <stdio.h>

/* count lines in input  */
/* main() */
/* { */
    /* // exercise 1-8 */
    /* int c, nl, b, t; */

    /* t = b = nl = 0; */
    /* while ((c = getchar()) != EOF) { */
        /* if (c == '\n') */
            /* ++nl; */
        /* if (c == ' ') */
            /* ++b; */
        /* if (c == '\t') */
            /* ++t; */
    /* } */
    /* printf("%d\n", nl); */
    /* printf("%d\n", t); */
    /* printf("%d\n", b); */
/* } */

/* write a program to copy its input to its output,  */
/* replacing each string of one or more blanks by a single blank */
/* main() */
/* { */
    /* // exercise 1-9 */
    /* int c, space; */
    /* space = 0; */

    /* while ((c = getchar()) != EOF) { */
        /* while (c == ' ') { */
            /* if (space == 0) { */
                /* putchar(c); */
                /* space = 1; */
            /* } */
            /* c = getchar(); */
        /* } */
        /* putchar(c); */
        /* space = 0; */
    /* } */
/* } */

/* write a program to copy its input to its output, replacing */
/* each tab by \t, each backspace by \b and each backslash by \\. */
/* this makes tabs and backspaces visible in an unambiguous way */
main()
{
    int c;

    while ((c = getchar()) != EOF) {
        if (c != '\t') 
            if (c != '\b') 
                if (c != '\\') 
                    putchar(c);
        if (c == '\t')
            printf("\\t");
        if (c == '\b')
            printf("\\b");
        if (c == '\\')
            printf("\\\\");
    }
}
