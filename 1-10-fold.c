#include <stdio.h>

/* exercise 1-22 */
/* write a program to fold long input lines
into to or more shorter lines after the last
non-blank character that occurs before the n-th
column of input
 */

#define MAX_COLUMN 20
#define MAXLINE 1000

char line[MAXLINE];
char result[MAXLINE];

void get_line(void);
void fold_line(void);

int main()
{
    get_line();
    fold_line();
    printf("%s\n", result);
}

void get_line(void)
{
    int c, i;
    for (i = 0; (c = getchar()) != EOF && c != '\n'; i++) {
        line[i] = c;
    }

    line[i] = '\0';
}

void fold_line(void)
{
    int col, i;
    int index_before;
    int word_length = 0;
    int result_index = 0;
    int current_line_length = 0;
    for (i = 0; i < MAXLINE && line[i] != '\0'; i++) {
        // save the current index
        index_before = i;
        // find the length of the next word
        while (line[i] != ' ' && line[i] != '\0')
            i++;

        word_length = i - index_before;
        // if the length of the next word is greater than the MAX_COLUMN
        // put the word in the result string and a newline character
        // else just put the word
        if (current_line_length + word_length > MAX_COLUMN) {
            result[result_index] = '\n';
            result_index++;
            current_line_length = 0;
        }

        for (int k = 0; k < word_length; k++) {
            result[k + result_index] = line[k + index_before];
        }
        result_index += word_length;
        result[result_index] = ' ';
        result_index++;
        current_line_length += word_length + 1;
    }

    result[result_index] = '\0';
}
