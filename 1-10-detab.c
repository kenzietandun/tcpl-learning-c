#include <stdio.h>

#define NUMSPACES 5
#define MAXLINE 1000

char line[MAXLINE];

void detab(void);
void entab(void);

int main()
{
    entab();
    printf("%s", line);
}

void detab(void)
{
    int c, i; 
    while ((c = getchar()) != EOF && c != '\n') {
        if (c == '\t') {
            for (int j = 0; j < NUMSPACES; j++)
                putchar(' ');
        } else {
            putchar(c);
        }
    }
}

void entab(void)
{
    int c, i, j;
    int should_replace = 0;

    char read[MAXLINE];

    for (i = 0; i < MAXLINE - 1 && (c = getchar()) != EOF && c != '\n'; i++) {
        read[i] = c;
    }
    if (c == '\n') {
        read[i] = c;
        i++;
    }
    read[i] = '\0';
    printf("%d\n", i);

    j = 0;
    for (i = 0; read[i] != '\0'; i++) {
        printf("%d %d\n", i, j);
        should_replace = 0;
        if (read[i] == ' ') {
            should_replace = 1;
            for (int k = 0; k < NUMSPACES; k++) {
                if (read[i+k] != ' ' || read[i+k] == '\0')
                    should_replace = 0;
            }
        }

        if (should_replace) {
            line[j] = '\t';
            i += NUMSPACES - 1;
        } else {
            line[j] = read[i];
        }
        j++;
    }

    line[j] = '\0';
}
