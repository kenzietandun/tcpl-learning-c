/* Word counting */

// exercise 1-11
/* any input that has words separated by fullstop or comma but */
/* not spaces would count as a word. */
/* e.g. */
/* 2.words. */
/* would be counted as 1 word when it should be 2 */

// exercise 1-12
/* write a program that prints its input one word per line */

#include <stdio.h>

#define IN  1
#define OUT 0
main()
{
    int c, state;

    while ((c = getchar()) != EOF) {
        while (c == ' ' || c == '\t' || c == '\n') {
            state = OUT;
            c = getchar();
        }
        if (state == OUT) {
            printf("\n");
            state = IN;
        }
        putchar(c);
    }
}
