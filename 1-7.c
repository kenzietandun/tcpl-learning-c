#include <stdio.h>

/* int power(int m, int n); */
int celsius_to_fahr(int celcius);

/* test power function */
/* main() */
/* { */
    /* int i; */
    /* for (i = 0; i < 10; i++) */
        /* printf("%3d %3d %3d\n", i, power(2, i), power(-3, i)); */
    /* return 0; */
/* } */

/* power: raise base to n-th power; n >= 0 */
/* int power(int base, int n) */
/* { */
    /* int i, p; */

    /* p = 1; */
    /* for (i = 1; i <= n; ++i) */
        /* p = p * base; */
    /* return p; */
/* } */

int celsius_to_fahr(int celsius)
{
    int fahr;
    fahr = (celsius + 32) * (9.0 / 5.0);
    return fahr;
}

main()
{
    float celsius, fahr;
    int lower, upper, step;

    lower = 20;
    upper = 300;
    step = 20;

    celsius = lower;
    while (celsius <= upper) {
        fahr = celsius_to_fahr(celsius);
        printf("%3.0f %6.1f\n", celsius, fahr);
        celsius += step; 
    }
}
